import React from "react";

import {BrowserRouter, Switch, Route} from "react-router-dom";
import { Provider } from 'react-redux';

import {createBrowserHistory} from "history";
import Header from "./components/Header";
import rootRoutes from "./rootRoutes";

import store from './redux/store';
import "./App.scss";

export const history = createBrowserHistory();
const supportsHistory = "pushState" in window.history;

function App() {
    return (
        <div className="App">
            <Provider store={store}>
                <BrowserRouter basename="/" forceRefresh={!supportsHistory}>
                    <Header/>
                    <Switch>
                        {rootRoutes.map((route, key) => {
                            return <Route key={key} {...route} />;
                        })}
                    </Switch>
                </BrowserRouter>
            </Provider>
        </div>
    );
}

export default App;
