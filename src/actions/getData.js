export const REQ_POSTS = "REQ_POSTS";
export const RES_POSTS = "RES_POSTS";
export const ERR_POSTS = "ERR_POSTS";

export const fetchPostsData = (data) => (dispatch, getState) => {
    dispatch({
        type: "PROMISE",
        actions: [
            REQ_POSTS,
            RES_POSTS,
            ERR_POSTS
        ],
        promise: fetch(`${data}`)
    });
};
