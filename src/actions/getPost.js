export const REQ_POST = "REQ_POST";
export const RES_POST = "RES_POST";
export const ERR_POST = "ERR_POST";

export const fetchPostData = (data) => (dispatch, getState) => {
    dispatch({
        type: "PROMISE",
        actions: [
            REQ_POST,
            RES_POST,
            ERR_POST
        ],
        promise: fetch(`${data}`)
    });
};
