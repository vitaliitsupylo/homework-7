import React from "react";
import {Link} from "react-router-dom";
import "./style.scss";

const Header = () => {
    return (
        <header className="header">
            <div className="header__wrap">
                <Link to="/" className="header__logo">
                    Logo
                </Link>
            </div>
        </header>
    );
};

export default Header;
