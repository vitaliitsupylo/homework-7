const promise = store => next => action => {

    if (action.type === "PROMISE") {
        const {dispatch} = store;
        const [REQ_ACTION, RES_ACTION, ERR_ACTION] = action.actions;

        dispatch({type: REQ_ACTION, middleware: true});
        return action.promise
            .then(res => res.json())
            .then(res => {
                dispatch({type: RES_ACTION, middleware: true, payload: res});
            }).catch(err => {
                dispatch({type: ERR_ACTION});
            });
    }

    return next(action);
};


export default promise;
