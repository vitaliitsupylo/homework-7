import React from "react";

const Error = () => {
    return (
        <h2 style={{fontSize: '50px', textAlign: 'cevnter', padding: '30px 0', color: 'red'}}>
            Page not found
        </h2>
    )
};

export default Error;