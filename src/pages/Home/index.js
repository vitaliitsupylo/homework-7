import React, {Component} from "react";
import {connect} from 'react-redux';

import {fetchPostsData} from './../../actions';

import Post from "./../../components/Post";
import Loading from "./../../components/Loading";
import Button from "../../components/Button";

class Home extends Component {
    state = {
        limit: 50,
        step: 25
    };

    componentDidMount() {
        this.props.fetchData(`https://jsonplaceholder.typicode.com/posts`)
    }

    loadMore = () => {
        const {limit, step} = this.state;
        const {data} = this.props;
        let changedLimit = limit + step;
        let unfinished = true;

        if (changedLimit >= data.length) {
            unfinished = false;
        }

        this.setState({
            limit: changedLimit,
            more: unfinished
        });
    };

    render() {
        const {limit} = this.state;
        const {data, loaded, error} = this.props;
        const {loadMore} = this;
        const count = data.length && limit;
        const listPost = data.filter((_, index) => index < count);

        return (
            <div className="page">
                <div className="page__wrap">
                    <div className="page__list">
                        {!loaded ? <Loading/> :
                            listPost.map((post, index) =>
                                <Post
                                    index={index}
                                    title={post.title}
                                    body={post.body}
                                    postUrl={`/posts/${post.id}`}
                                    key={post.id}
                                    id={post.id}
                                />)}
                    </div>
                    {data.length > limit ? <Button handler={loadMore}>More</Button> : null}
                    {!error && <p>An error occurred while downloading posts</p>}
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    data: state.posts.data,
    loaded: state.posts.loaded,
    error: state.posts.error
});

const mapDispatchToProps = (dispatch) => ({
    fetchData: (url) => {
        dispatch(fetchPostsData(url));
    }
});


export default connect(mapStateToProps, mapDispatchToProps)(Home);
