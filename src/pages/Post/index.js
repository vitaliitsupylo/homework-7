import React, {Component} from "react";
import {Link} from "react-router-dom";
import {connect} from 'react-redux';

import {fetchPostData} from './../../actions';
import Loading from "./../../components/Loading";

class PostPage extends Component {

    componentDidMount() {
        const {match, history} = this.props;
        const {postId} = match.params;

        if (Number.isInteger(+postId)) {
            this.props.fetchData(`https://jsonplaceholder.typicode.com/posts/${postId}`)
        } else {
            history.push("/");
        }
    }

    render() {
        const {data, loaded, error} = this.props;
        return (
            <div className="page">
                <div className="page__wrap">
                    {!loaded ?
                        <Loading/> :
                        <article className="post-full">
                            <h1 className="post-full__title">{data.title}</h1>
                            <p className="post-full__body">{data.body}</p>
                            <Link to={`/user/${data.id}`} className="post__link">
                                <span>All user posts</span>
                            </Link>
                        </article>
                    }
                </div>
            </div>
        );
    }
}


const mapStateToProps = (state) => ({
    data: state.post.data,
    loaded: state.post.loaded,
    error: state.post.error
});

const mapDispatchToProps = (dispatch) => ({
    fetchData: (url) => {
        dispatch(fetchPostData(url));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(PostPage);

