import React, {Component} from "react";
import Post from "./../../components/Post";
import Loading from "./../../components/Loading";
import {fetchPostsData} from "../../actions";
import {connect} from "react-redux";

class UserPosts extends Component {

    componentDidMount() {
        const {match, history} = this.props;
        const {postId} = match.params;

        if (Number.isInteger(+postId)) {
            this.props.fetchData(`https://jsonplaceholder.typicode.com/posts?userId=${postId}`)
        } else {
            history.push("/");
        }
    }

    render() {
        const {data, loaded, error} = this.props;
        return (
            <div className="page">
                <div className="page__wrap">
                    <div className="page__list">
                        {!loaded ? <Loading/> : data.map((post, index) =>
                            <Post
                                index={index}
                                title={post.title}
                                body={post.body}
                                postUrl={`/posts/${post.id}`}
                                key={post.id}
                                id={post.id}
                            />)}
                        {loaded && data.length === 0 && <p>Such user does not exist</p>}
                        {!error && <p>An error occurred while downloading posts</p>}
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    data: state.posts.data,
    loaded: state.posts.loaded,
    error: state.posts.error
});

const mapDispatchToProps = (dispatch) => ({
    fetchData: (url) => {
        dispatch(fetchPostsData(url));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(UserPosts);
