import {
    REQ_POST,
    RES_POST,
    ERR_POST
} from '../../actions';

const postInitialState = {
    loading: false,
    loaded: false,
    data: null,
    error: false
};

const postReducer = (state = postInitialState, action) => {
    switch (action.type) {
        case REQ_POST:
            return {
                ...state,
                loading: true,
                loaded: false
            };

        case RES_POST:
            return {
                ...state,
                loaded: true,
                loading: false,
                data: action.payload
            };
        case ERR_POST:
            return {
                ...state,
                loaded: true,
                loading: false,
                error: true
            };

        default:
            return state;
    }
};

export default postReducer;
