import {
    REQ_POSTS,
    RES_POSTS,
    ERR_POSTS
} from '../../actions';

const postsInitialState = {
    loading: false,
    loaded: false,
    data: [],
    error: false
};

const postsReducer = (state = postsInitialState, action) => {
    switch (action.type) {
        case REQ_POSTS:
            return {
                ...state,
                loading: true,
                loaded: false
            };

        case RES_POSTS:
            return {
                ...state,
                loaded: true,
                loading: false,
                data: action.payload
            };
        case ERR_POSTS:
            return {
                ...state,
                loaded: true,
                loading: false,
                error: true
            };

        default:
            return state;
    }
};

export default postsReducer;
