import Post from "./pages/Post";
import Error from './pages/Error';
import Home from "./pages/Home";
import UserPosts from "./pages/UserPosts";

const rootRoutes = [
    {
        path: "/",
        component: Home,
        exact: true
    },
    {
        path: "/posts/:postId",
        exact: true,
        component: Post
    },
    {
        path: "/user/:postId",
        exact: true,
        component: UserPosts
    },
    {
        component: Error
    }
];

export default rootRoutes;
